# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


def get_manager():
    return models.Manager().db_manager('groundwater')


class GwOuvCadast(models.Model):
    id_cadast = models.AutoField(primary_key=True)
    ref_ouvrage = models.ForeignKey(
        'GwOuv',
        models.DO_NOTHING,
        db_column='ref_ouvrage',
        blank=True,
        null=True)
    ref_sol_polue = models.IntegerField(blank=True, null=True)
    nr_parcelle = models.CharField(max_length=21, blank=True, null=True)
    ref_chantier = models.IntegerField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_cadast'


class GwOuvMulti(models.Model):
    id_ouvrage_multi = models.AutoField(primary_key=True)
    ref_ouvrage = models.ForeignKey(
        'GwOuv',
        models.DO_NOTHING,
        db_column='ref_ouvrage',
        blank=True,
        null=True)
    ref_type_ouvrage = models.IntegerField(blank=True, null=True)
    nombre = models.IntegerField(blank=True, null=True)
    prof_rel_m = models.CharField(max_length=10, blank=True, null=True)
    top_crepine_rel_m = models.CharField(max_length=10, blank=True, null=True)
    prof_crepine_rel_m = models.CharField(max_length=10, blank=True, null=True)
    diametre_mm = models.CharField(max_length=10, blank=True, null=True)
    nom_ouv = models.CharField(max_length=40, blank=True, null=True)
    commentaire = models.TextField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_multi'


class GwOuvTcommune(models.Model):
    id_commune = models.AutoField(primary_key=True)
    code_ins = models.CharField(max_length=3, blank=True, null=True)
    commune = models.CharField(max_length=50, blank=True, null=True)
    code_postal = models.IntegerField()
    region = models.CharField(max_length=30, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_tcommune'


class GwOuvTetat(models.Model):
    id_etat = models.AutoField(primary_key=True)
    etat_ouvrage = models.CharField(max_length=50, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_tetat'


class GwOuvTnetworkpz(models.Model):
    id_network_pz = models.AutoField(primary_key=True)
    network_pz = models.CharField(max_length=50, blank=True, null=True)
    commentaire = models.CharField(max_length=255, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_tnetworkpz'


class GwOuvTref(models.Model):
    id_ref_mes = models.AutoField(primary_key=True)
    ref_mes = models.CharField(max_length=50, blank=True, null=True)
    commentaire = models.TextField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_tref'


class GwOuvTtype(models.Model):
    id_type = models.AutoField(primary_key=True)
    type_ouvrage = models.CharField(max_length=50, blank=True, null=True)
    commentaire = models.CharField(max_length=255, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv_ttype'


class GwOuv(models.Model):
    id_ouvrage = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=40, blank=True, null=True)
    ref_etat_ouvrage = models.ForeignKey(
        'GwOuvTetat',
        models.DO_NOTHING,
        db_column='ref_etat_ouvrage',
        blank=True,
        null=True)
    ref_network_pz = models.ForeignKey(
        'GwOuvTnetworkpz',
        models.DO_NOTHING,
        db_column='ref_network_pz',
        blank=True,
        null=True)
    ref_reference_mes = models.IntegerField(blank=True, null=True)
    comm_ref_mes = models.CharField(max_length=100, blank=True, null=True)
    ref_type_ouvrage = models.ForeignKey(
        'GwOuvTtype',
        models.DO_NOTHING,
        db_column='ref_type_ouvrage',
        blank=True,
        null=True)
    date_forage = models.DateField(blank=True, null=True)
    x_ori_m = models.FloatField(blank=True, null=True)
    x_map_m = models.FloatField(blank=True, null=True)
    x_gps_m = models.FloatField(blank=True, null=True)
    x_ref_m = models.FloatField(blank=True, null=True)
    y_ori_m = models.FloatField(blank=True, null=True)
    y_map_m = models.FloatField(blank=True, null=True)
    y_gps_m = models.FloatField(blank=True, null=True)
    erreur_xy_gps_m = models.FloatField(blank=True, null=True)
    y_ref_m = models.FloatField(blank=True, null=True)
    z_ori_m = models.FloatField(blank=True, null=True)
    z_lidar_m = models.FloatField(blank=True, null=True)
    correct_z_lidar_m = models.FloatField(blank=True, null=True)
    correct_z_gps_m = models.FloatField(blank=True, null=True)
    z_gps_m = models.FloatField(blank=True, null=True)
    erreur_z_gps_m = models.FloatField(blank=True, null=True)
    z_ref_m = models.FloatField(blank=True, null=True)
    prof_rel_m = models.FloatField(blank=True, null=True)
    top_crepine_rel_m = models.FloatField(blank=True, null=True)
    prof_crepine_rel_m = models.FloatField(blank=True, null=True)
    top_gravier_rel_m = models.FloatField(blank=True, null=True)
    prof_gravier_rel_m = models.FloatField(blank=True, null=True)
    diametre_mm = models.IntegerField(blank=True, null=True)
    commentaire = models.TextField(blank=True, null=True)
    ref_pid_pz_stib = models.IntegerField(blank=True, null=True)
    adresse_1 = models.CharField(max_length=150, blank=True, null=True)
    adresse_2 = models.CharField(max_length=150, blank=True, null=True)
    adresse_3 = models.CharField(max_length=150, blank=True, null=True)
    ref_cp = models.IntegerField(blank=True, null=True)
    comm_emplacement = models.CharField(max_length=255, blank=True, null=True)
    comm_acces = models.CharField(max_length=255, blank=True, null=True)
    the_geom = models.PointField(srid=31370, dim=3, blank=True, null=True)
    nr_from = models.CharField(max_length=255, blank=True, null=True)
    nr_to = models.CharField(max_length=255, blank=True, null=True)
    nr_boite = models.CharField(max_length=255, blank=True, null=True)
    nr_parcelle_xy = models.CharField(max_length=21, blank=True, null=True)
    aff = models.CharField(max_length=10000, blank=True, null=True)
    naam = models.CharField(max_length=40, blank=True, null=True)
    adres_1 = models.CharField(max_length=150, blank=True, null=True)
    confstatus = models.CharField(max_length=1, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_ouv'


class GwMnetQual(models.Model):
    id_qual = models.AutoField(primary_key=True)
    ref_ouvrage = models.ForeignKey(
        'GwOuv', models.DO_NOTHING, db_column='ref_ouvrage')
    commentaire = models.CharField(max_length=255, blank=True, null=True)
    no_station = models.IntegerField()
    no_code = models.CharField(max_length=50, blank=True, null=True)
    no_eu_code = models.CharField(max_length=50, blank=True, null=True)
    station_mes = models.CharField(max_length=50, blank=True, null=True)
    start_mon = models.DateField(blank=True, null=True)
    end_mon = models.DateField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_mnet_qual'


class GwMnetQualDesc(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_qual = models.IntegerField(blank=True, null=True)
    ref_ouvrage = models.IntegerField(blank=True, null=True)
    no_station = models.IntegerField(blank=True, null=True)
    no_code = models.CharField(max_length=50, blank=True, null=True)
    no_eu_code = models.CharField(max_length=50, blank=True, null=True)
    station_mes = models.CharField(max_length=50, blank=True, null=True)
    type_ouvrage = models.CharField(max_length=50, blank=True, null=True)
    start_mon = models.DateField(blank=True, null=True)
    end_mon = models.DateField(blank=True, null=True)
    ref_gwb = models.IntegerField(blank=True, null=True)
    code_gwb = models.CharField(max_length=50, blank=True, null=True)
    ref_gw_ref = models.IntegerField(blank=True, null=True)
    nom_gw_ref = models.CharField(max_length=100, blank=True, null=True)
    x_ref_m = models.FloatField(blank=True, null=True)
    y_ref_m = models.FloatField(blank=True, null=True)
    z_ref_m = models.FloatField(blank=True, null=True)
    adresse_1 = models.CharField(max_length=150, blank=True, null=True)
    ref_cp = models.IntegerField(blank=True, null=True)
    prof_rel_m = models.FloatField(blank=True, null=True)
    top_crepine_rel_m = models.FloatField(blank=True, null=True)
    prof_crepine_rel_m = models.FloatField(blank=True, null=True)
    commentaire = models.TextField(blank=True, null=True)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    naam_gw_ref = models.CharField(max_length=100, blank=True, null=True)
    nom_gwb_fr = models.CharField(max_length=50, blank=True, null=True)
    naam_gwb_nl = models.CharField(max_length=50, blank=True, null=True)
    data = models.CharField(max_length=200, blank=True, null=True)
    data_nl = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_mnet_qual_desc'


class GwQualEchanti(models.Model):
    id_ech = models.IntegerField(primary_key=True)
    date_ech = models.DateField(blank=True, null=True)
    code_ech = models.CharField(max_length=50, blank=True, null=True)
    code_rapport = models.ForeignKey(
        'GwQualRapport',
        models.DO_NOTHING,
        db_column='code_rapport',
        blank=True,
        null=True)
    code_station = models.IntegerField(blank=True, null=True)
    commentaires = models.CharField(max_length=500, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_echanti'


class GwQualMethode(models.Model):
    idmethode = models.IntegerField(primary_key=True)
    nommethode = models.CharField(max_length=100, blank=True, null=True)
    reference_doc = models.CharField(max_length=100, blank=True, null=True)
    uncertainty = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True, null=True)
    loq = models.DecimalField(
        max_digits=10, decimal_places=4, blank=True, null=True)
    lod = models.DecimalField(
        max_digits=10, decimal_places=4, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_methode'


class GwQualRapport(models.Model):
    no = models.IntegerField(primary_key=True)
    ref_rapport = models.CharField(max_length=50, blank=True, null=True)
    nomlaboratoire = models.CharField(max_length=50, blank=True, null=True)
    rapportoriginal = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_rapport'


class GwQualGrouppar(models.Model):
    idgroupe = models.IntegerField(primary_key=True)
    nomgroupe = models.CharField(max_length=100, blank=True, null=True)
    group_sdi = models.BooleanField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_grouppar'


class GwQualParametre(models.Model):
    idparametre = models.IntegerField(primary_key=True)
    nomparametre = models.CharField(max_length=100, blank=True, null=True)
    uniteparametre = models.CharField(max_length=50, blank=True, null=True)
    numerocas = models.CharField(max_length=15, blank=True, null=True)
    normedce_nqe_br01 = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    norme_eaupotable = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    normedce_nqe_br02 = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    normedce_nqe_br03 = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    normedce_nqe_br04 = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    normedce_nqe_br05 = models.DecimalField(
        max_digits=10, decimal_places=3, blank=True, null=True)
    naamparametre = models.CharField(max_length=100, blank=True, null=True)
    name_parametre = models.CharField(max_length=100, blank=True, null=True)
    authorised_by2016 = models.CharField(max_length=10, blank=True, null=True)
    idusage = models.ForeignKey(
        'GwQualUsage',
        models.DO_NOTHING,
        db_column='idusage',
        blank=True,
        null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_parametre'


class GwQualResultat(models.Model):
    gid = models.AutoField(primary_key=True)
    idechantillon = models.ForeignKey(
        GwQualEchanti,
        models.DO_NOTHING,
        db_column='idechantillon',
        blank=True,
        null=True)
    idparametre = models.ForeignKey(
        GwQualParametre,
        models.DO_NOTHING,
        db_column='idparametre',
        blank=True,
        null=True)
    valeurnum = models.FloatField(blank=True, null=True)
    valeurtexte = models.CharField(max_length=50, blank=True, null=True)
    commentaires = models.CharField(max_length=500, blank=True, null=True)
    methodemesure = models.ForeignKey(
        GwQualMethode,
        models.DO_NOTHING,
        db_column='methodemesure',
        blank=True,
        null=True)
    commentaire_interne = models.CharField(
        max_length=500, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_resultat'


class GwQualUsage(models.Model):
    idusage = models.IntegerField(primary_key=True)
    nom_usage = models.CharField(max_length=100, blank=True, null=True)
    naam_usage = models.CharField(max_length=100, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_usage'


class GwQualIngrouppar(models.Model):
    gid = models.AutoField(primary_key=True)
    noidgroupeparametre = models.ForeignKey(
        GwQualGrouppar,
        models.DO_NOTHING,
        db_column='noidgroupeparametre',
        blank=True,
        null=True)
    comprenant_parametres = models.ForeignKey(
        GwQualParametre,
        models.DO_NOTHING,
        db_column='comprenant_parametres',
        blank=True,
        null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_ingrouppar'


class GwTprogram(models.Model):
    id_type_program = models.AutoField(primary_key=True)
    program_type = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_tprogram'


class GwQualProgram(models.Model):
    id_program = models.AutoField(primary_key=True)
    no_station = models.IntegerField()
    type_program = models.ForeignKey(
        'GwTprogram',
        models.DO_NOTHING,
        db_column='type_program',
        blank=True,
        null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_program'


class GwQualResultatNumloq2(models.Model):
    gid = models.IntegerField(blank=True, null=True)
    idechantillon = models.IntegerField(blank=True, null=True)
    idparametre = models.IntegerField(blank=True, null=True)
    valeurnum = models.FloatField(blank=True, null=True)
    valeurtexte = models.CharField(max_length=50, blank=True, null=True)
    commentaires = models.CharField(max_length=500, blank=True, null=True)
    methodemesure = models.IntegerField(blank=True, null=True)
    valeurnum_loq2 = models.DecimalField(
        max_digits=10, decimal_places=4, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_qual_resultat_numloq2'


class GwQualStation(models.Model):
    nostation = models.IntegerField(primary_key=True)
    noeucode = models.CharField(max_length=50, blank=True, null=True)
    nocode = models.CharField(max_length=10, blank=True, null=True)
    captid = models.IntegerField(blank=True, null=True)
    nom_station = models.CharField(max_length=50, blank=True, null=True)
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    adresse_station = models.CharField(max_length=50, blank=True, null=True)
    cp = models.IntegerField(blank=True, null=True)
    type_station = models.CharField(max_length=50, blank=True, null=True)
    surv_qte = models.IntegerField(blank=True, null=True)
    usage_eau = models.CharField(max_length=50, blank=True, null=True)
    prof_puits = models.CharField(max_length=50, blank=True, null=True)
    prof_crepine = models.CharField(max_length=50, blank=True, null=True)
    prof_pompe = models.FloatField(blank=True, null=True)
    prof_prelev = models.CharField(max_length=50, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_qual_station'


class GwMnetPiezoQuantDesc(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_ouvrage = models.IntegerField(blank=True, null=True)
    nom = models.CharField(max_length=40, blank=True, null=True)
    mon_num = models.IntegerField(blank=True, null=True)
    mon_be_code = models.CharField(max_length=40, blank=True, null=True)
    mon_eu_code = models.CharField(max_length=40, blank=True, null=True)
    ref_gwb = models.IntegerField(blank=True, null=True)
    code_gwb = models.CharField(max_length=50, blank=True, null=True)
    ref_gw_ref = models.IntegerField(blank=True, null=True)
    nom_gw_ref = models.CharField(max_length=100, blank=True, null=True)
    start_mon = models.DateField(blank=True, null=True)
    end_mon = models.DateField(blank=True, null=True)
    mon_freq = models.CharField(max_length=40, blank=True, null=True)
    x_ref_m = models.FloatField(blank=True, null=True)
    y_ref_m = models.FloatField(blank=True, null=True)
    z_ref_m = models.FloatField(blank=True, null=True)
    adresse_1 = models.CharField(max_length=150, blank=True, null=True)
    ref_cp = models.IntegerField(blank=True, null=True)
    prof_rel_m = models.FloatField(blank=True, null=True)
    top_crepine_rel_m = models.FloatField(blank=True, null=True)
    prof_crepine_rel_m = models.FloatField(blank=True, null=True)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    naam_gw_ref = models.CharField(max_length=100, blank=True, null=True)
    nom_gwb_fr = models.CharField(max_length=50, blank=True, null=True)
    naam_gwb_nl = models.CharField(max_length=50, blank=True, null=True)
    data = models.CharField(max_length=255, blank=True, null=True)
    data_nl = models.CharField(max_length=255, blank=True, null=True)
    mon_type = models.CharField(max_length=150, blank=True, null=True)
    type_program = models.IntegerField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_mnet_piezo_quant_desc'


class CacheMiss(Exception):
    pass


class PiezoModelCache:
    _data = dict()

    @classmethod
    def push(cls, id, model):
        cls._data[id] = model
        return model

    @classmethod
    def find(cls, id):
        if id in cls._data:
            return cls._data[id]
        raise CacheMiss()


def make_prefix(s):
    return lambda x: '{}_{}'.format(s, x)


def piezo_model(code):
    """Here it'a a bit embarassing, data has been put in distinct tables.
    Therefore we need to build dynamic models. 
    And they made it even funnier by adding the identifier to column names :/
    """

    try:
        return PiezoModelCache.find(code)
    except CacheMiss:
        class_name = 'PiezoDataDyn_{}'.format(code)
        table_name = 'pz_data_{}'.format(code)
        prefix = make_prefix('data_{}'.format(code))
        meta = type(
            'Meta', (),
            dict(managed=False, db_table=table_name, app_label='water'))
        model_fields = {
            'Meta':
            meta,
            '__module__':
            'water.models',
            'objects':
            get_manager(),
            prefix('time'):
            models.DateTimeField(primary_key=True),
            prefix('hp'):
            models.DecimalField(
                max_digits=7, decimal_places=2, blank=True, null=True),
            prefix('pfsol'):
            models.DecimalField(
                max_digits=7, decimal_places=2, blank=True, null=True),
            prefix('tv'):
            models.SmallIntegerField(blank=True, null=True),
        }

        return PiezoModelCache.push(
            code, type(class_name, (models.Model, ), model_fields))


class GwHgeolTgwb(models.Model):
    id_gwb = models.AutoField(primary_key=True)
    code_gwb = models.CharField(max_length=50, blank=True, null=True)
    nom_gwb_fr = models.CharField(max_length=50, blank=True, null=True)
    naam_gwb_nl = models.CharField(max_length=50, blank=True, null=True)
    niveau = models.CharField(max_length=50, blank=True, null=True)
    serie_geol_fr = models.CharField(max_length=50, blank=True, null=True)
    serie_geol_nl = models.CharField(max_length=50, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'gw_hgeol_tgwb'
