# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


def get_manager():
    return models.Manager().db_manager('surfacewater')


class Campaign(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_campaign = models.CharField(max_length=200)
    lab_name = models.CharField(max_length=200, blank=True, null=True)
    campaign_year = models.IntegerField()
    campaign_com = models.CharField(max_length=200, blank=True, null=True)
    remarks = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'campaign'


class Correction(models.Model):
    gid = models.AutoField(primary_key=True)
    id_result = models.ForeignKey(
        'Results', models.DO_NOTHING, db_column='id_result')
    error_type = models.CharField(max_length=2000, blank=True, null=True)
    correction_com = models.CharField(max_length=2000, blank=True, null=True)
    bruenvi_created = models.DateTimeField(blank=True, null=True)
    bruenvi_author = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'correction'


class EtatPc(models.Model):
    gid_site = models.CharField(max_length=200, blank=True, null=True)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    x_lamb72 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    y_lamb72 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    id_waterbody = models.CharField(max_length=200, blank=True, null=True)
    id_par_result = models.CharField(max_length=200, blank=True, null=True)
    par_fr = models.CharField(max_length=200, blank=True, null=True)
    avg_2015_2017 = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    p90_2015_2017 = models.FloatField(blank=True, null=True)
    min_2015_2017 = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    max_2015_2017 = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    count = models.BigIntegerField(blank=True, null=True)
    norms_aa_bcr = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_aa_eu = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_sum = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_mac = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_min_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_max_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_aa_min_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_p90 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    norms_max_count = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    etat = models.CharField(max_length=200, blank=True, null=True)
    law = models.CharField(max_length=2000, blank=True, null=True)
    classes = models.CharField(max_length=2000, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'etat_pc'


class Group(models.Model):
    gid = models.IntegerField(primary_key=True)
    group_short = models.CharField(max_length=200, blank=True, null=True)
    group_fr = models.CharField(max_length=2000, blank=True, null=True)
    group_nl = models.CharField(max_length=2000, blank=True, null=True)
    group_en = models.CharField(max_length=2000, blank=True, null=True)
    group_sdi = models.BooleanField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'group'


class GroupPar(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_par_result = models.ForeignKey(
        'ParResult', models.DO_NOTHING, db_column='id_par_result')
    id_group = models.ForeignKey(
        Group, models.DO_NOTHING, db_column='id_group')
    subgroup = models.CharField(max_length=200, blank=True, null=True)
    e_prtr_water_rls_tresh = models.CharField(
        max_length=200, blank=True, null=True)
    remarks = models.CharField(max_length=2000, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'group_par'


class Matrix(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_matrix = models.CharField(max_length=200)
    matrix_fr = models.CharField(max_length=200, blank=True, null=True)
    matrix_nl = models.CharField(max_length=200, blank=True, null=True)
    matrix_en = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'matrix'


class Norms(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_par_result = models.ForeignKey(
        'ParResult',
        models.DO_NOTHING,
        db_column='id_par_result',
        blank=True,
        null=True)
    id_waterbody = models.ForeignKey(
        'Waterbody', models.DO_NOTHING, db_column='id_waterbody')
    aa_bcr = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    aa_eu = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    sum = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    mac = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    min_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    max_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    aa_min_val = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    biote = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    summer = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    winter = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    p10 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    p90 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    max_count_year = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    com = models.CharField(max_length=200, blank=True, null=True)
    law = models.CharField(max_length=2000, blank=True, null=True)
    classes = models.CharField(max_length=2000, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'norms'


class ParResult(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_par_result = models.CharField(max_length=200, blank=True, null=True)
    id_par = models.ForeignKey(
        'Parameter',
        models.DO_NOTHING,
        db_column='id_par',
        blank=True,
        null=True)
    frac = models.CharField(max_length=200, blank=True, null=True)
    id_matrix = models.ForeignKey(
        Matrix,
        models.DO_NOTHING,
        db_column='id_matrix',
        blank=True,
        null=True)
    result_unit = models.CharField(max_length=200, blank=True, null=True)
    min_poss = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    max_poss = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    min_warning = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    max_warning = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'par_result'


class Parameter(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_par = models.CharField(max_length=200, blank=True, null=True)
    short_name = models.CharField(max_length=200, blank=True, null=True)
    par_fr = models.CharField(max_length=200, blank=True, null=True)
    par_nl = models.CharField(max_length=200, blank=True, null=True)
    par_en = models.CharField(max_length=200, blank=True, null=True)
    cas = models.CharField(max_length=200, blank=True, null=True)
    logkow = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    mol_formula = models.CharField(max_length=200, blank=True, null=True)
    bcf = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    bmf = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    id_source = models.CharField(max_length=200, blank=True, null=True)
    unit_param = models.CharField(max_length=200, blank=True, null=True)
    par_com = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'parameter'


class Point(models.Model):
    gid = models.AutoField(primary_key=True)
    gid_point = models.CharField(max_length=200, blank=True, null=True)
    gid_site = models.ForeignKey(
        'Site', models.DO_NOTHING, db_column='gid_site', blank=True, null=True)
    x_lamb72 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    y_lamb72 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    x_lab = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    y_lab = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    epsg_lab = models.IntegerField(blank=True, null=True)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    point_com = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'point'


class Results(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_lab_sample = models.CharField(max_length=200, blank=True, null=True)
    lab_result_nber = models.CharField(max_length=200, blank=True, null=True)
    id_sample = models.ForeignKey(
        'Sample',
        models.DO_NOTHING,
        db_column='id_sample',
        blank=True,
        null=True)
    id_par_result = models.ForeignKey(
        ParResult, models.DO_NOTHING, db_column='id_par_result')
    analysis_method = models.CharField(max_length=200, blank=True, null=True)
    site_lab = models.CharField(max_length=200)
    lab_value = models.CharField(max_length=200)
    correct_lab_value = models.CharField(max_length=200, blank=True, null=True)
    validated_txt_value = models.CharField(
        max_length=200, blank=True, null=True)
    val_num = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    val_flag = models.ForeignKey(
        'ValGrid',
        models.DO_NOTHING,
        db_column='val_flag',
        blank=True,
        null=True)
    val_com = models.CharField(max_length=2000, blank=True, null=True)
    analysis_date = models.DateTimeField(blank=True, null=True)
    lod = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    loq_bid = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    loq_report = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    lod_com = models.CharField(max_length=200, blank=True, null=True)
    loq_com = models.CharField(max_length=200, blank=True, null=True)
    u = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    import_date = models.DateTimeField(blank=True, null=True)
    remarks = models.CharField(max_length=200, blank=True, null=True)
    results_car = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'results'


class Sample(models.Model):
    gid = models.AutoField(primary_key=True)
    id_lab_sample = models.CharField(max_length=200, blank=True, null=True)
    gid_point = models.ForeignKey(
        Point, models.DO_NOTHING, db_column='gid_point', blank=True, null=True)
    sampling_date = models.DateField(blank=True, null=True)
    sampling_hour = models.TimeField(blank=True, null=True)
    id_matrix = models.ForeignKey(
        Matrix,
        models.DO_NOTHING,
        db_column='id_matrix',
        blank=True,
        null=True)
    sampling_method = models.CharField(max_length=200, blank=True, null=True)
    id_campaign = models.ForeignKey(
        Campaign,
        models.DO_NOTHING,
        db_column='id_campaign',
        blank=True,
        null=True)
    sampling_com = models.CharField(max_length=200, blank=True, null=True)
    last_update = models.DateTimeField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'sample'


class Site(models.Model):
    gid = models.IntegerField(primary_key=True)
    gid_site = models.CharField(max_length=200, blank=True, null=True)
    id_waterbody = models.ForeignKey(
        'Waterbody', models.DO_NOTHING, db_column='id_waterbody')
    lat_etrs89 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    long_etrs89 = models.DecimalField(
        max_digits=11, decimal_places=4, blank=True, null=True)
    x_lamb72 = models.DecimalField(max_digits=11, decimal_places=4)
    y_lamb72 = models.DecimalField(max_digits=11, decimal_places=4)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    eu_site = models.CharField(max_length=200, blank=True, null=True)
    site_fr = models.CharField(max_length=200, blank=True, null=True)
    site_nl = models.CharField(max_length=200, blank=True, null=True)
    site_en = models.CharField(max_length=200, blank=True, null=True)
    street_fr = models.CharField(max_length=200, blank=True, null=True)
    street_nl = models.CharField(max_length=200, blank=True, null=True)
    postcode = models.IntegerField(blank=True, null=True)
    commune_fr = models.CharField(max_length=200, blank=True, null=True)
    commune_nl = models.CharField(max_length=200, blank=True, null=True)
    com = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'site'


class SpatialVal(models.Model):
    index = models.IntegerField(blank=True, null=True)
    flag = models.CharField(max_length=200, blank=True, null=True)
    id_par_result = models.CharField(max_length=200, blank=True, null=True)
    out_of_range_zen_in = models.CharField(
        max_length=200, blank=True, null=True)
    out_of_range_zen_out = models.CharField(
        max_length=200, blank=True, null=True)
    remarks = models.CharField(max_length=200, blank=True, null=True)
    sampling_date = models.CharField(max_length=200, blank=True, null=True)
    the_geom = models.PointField(srid=31370, blank=True, null=True)
    zen_in = models.CharField(max_length=200, blank=True, null=True)
    zen_out = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'spatial_val'


class ValGrid(models.Model):
    gid = models.AutoField(primary_key=True)
    id_val_flag = models.CharField(max_length=200)
    flag_meaning = models.CharField(max_length=2000)
    representativeness = models.CharField(max_length=200)
    representativeness_meaning = models.CharField(max_length=200)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'val_grid'


class ValInfo(models.Model):
    gid = models.AutoField(primary_key=True)
    id_result = models.ForeignKey(
        Results, models.DO_NOTHING, db_column='id_result')
    bruenvi_created = models.DateTimeField(blank=True, null=True)
    bruenvi_author = models.CharField(max_length=200, blank=True, null=True)
    val_com = models.CharField(max_length=2000, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'val_info'


class Waterbody(models.Model):
    gid = models.IntegerField(primary_key=True)
    id_waterbody = models.CharField(max_length=200, blank=True, null=True)
    waterbody_fr = models.CharField(max_length=200, blank=True, null=True)
    waterbody_nl = models.CharField(max_length=200, blank=True, null=True)
    waterbody_en = models.CharField(max_length=200, blank=True, null=True)
    eu_waterbody_code = models.CharField(max_length=200, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'waterbody'


class NormNames(models.Model):
    norm_names = models.CharField(primary_key=True, max_length=200)
    fr = models.CharField(max_length=2000, blank=True, null=True)
    nl = models.CharField(max_length=2000, blank=True, null=True)
    en = models.CharField(max_length=2000, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False
        db_table = 'norm_names'
