NORM_KEYS = [
    'aa_bcr',
    'aa_eu',
    'sum',
    'mac',
    'min_val',
    'max_val',
    'aa_min_val',
    'biote',
    'summer',
    'winter',
    'p10',
    'p90',
]


def serialize_norm(norm):
    values = dict()
    for key in NORM_KEYS:
        val = getattr(norm, key)
        if val is not None:
            values[key] = float(val)

    return {
        'options': {
            'waterbody': norm.id_waterbody_id,
        },
        'values': values
    }


class CacheMiss(Exception):
    pass


class ParameterCache:
    _data = dict()

    @classmethod
    def push(cls, id, ser_param):
        cls._data[id] = ser_param

    @classmethod
    def find(cls, id):
        if id in cls._data:
            return cls._data[id]
        raise CacheMiss()


def serialize_parameter(param):
    """Serialize a models.surface.Parameter
    """
    try:
        return ParameterCache.find(param.gid)
    except CacheMiss:

        pr = param.parresult_set.first()  # fragile but it works
        groups = []
        norms = []
        if pr is not None:
            for gp in pr.grouppar_set.filter(id_group__group_sdi=True):
                group = gp.id_group  # so weird naming, this is not an id
                groups.append({
                    'id': group.gid,
                    'name': {
                        'fr': group.group_fr,
                        'nl': group.group_nl,
                    }
                })

            norms = [
                serialize_norm(n)
                for n in pr.norms_set.filter(end_date__isnull=True)
            ]

        frac = pr.frac if pr is not None else None

        def fname(n):
            if frac is not None:
                return '{} ({})'.format(n, frac)
            return n

        ser_param = {
            'id': param.gid,
            'code': param.short_name,
            'name': {
                'fr': fname(param.par_fr),
                'nl': fname(param.par_nl),
                'en': fname(param.par_en),
            },
            'unit': param.unit_param,
            'groups': groups,
            'norms': norms,
        }
        ParameterCache.push(param.gid, ser_param)

        return ser_param


def parse_float(s):
    result = float(s.replace(',', '.'))
    # print('parse_float({}) => {}'.format(s, result))
    return result


def serialize_timeserie_record(rec):
    """Serialize a models.surface.Results
    """
    txt = rec.validated_txt_value

    if txt.startswith('<'):
        return [
            rec.id_sample.sampling_date.isoformat(),
            parse_float(txt[1:]), 0.5
        ]

    return [rec.id_sample.sampling_date.isoformat(), parse_float(txt), 1]
