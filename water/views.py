from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound, FileResponse
from django.shortcuts import get_object_or_404
from django.core.serializers import serialize
from django.utils.text import slugify
from datetime import date
import json
from water import models, serializers
from water.config import parse_config
import itertools as it
import zipfile
import csv
from pathlib import PosixPath
from django.template.loader import render_to_string
import shutil
import random
import string
import tempfile
import re

from water.stat import get_statistics_surface, get_statistics_ground_quality, get_statistics_ground_quantity


def get_catalog_surface(request):
    ser = serializers.surface.serialize_parameter
    Parameter = models.surface.Parameter
    params = [ser(p) for p in Parameter.objects.all()]

    return JsonResponse(params, safe=False)


def get_catalog_ground_quality(request):
    ser = serializers.ground.serialize_parameter_quality
    Parameter = models.ground.GwQualParametre
    params = [ser(p) for p in Parameter.objects.all()]

    return JsonResponse(params, safe=False)


PIEZO_RELATIVE = 1
PIEZO_ABSOLUTE = 2


def get_catalog_ground_quantity(request):
    """Note that this is basically hardcoded here to make it consistent with the other catalogs, but client should know
    """
    piezo_rel = {
        'id': PIEZO_RELATIVE,
        'code': 'rel',
        'name': {
            'fr':
            'Profondeur du niveau d’eau par rapport à la surface du sol ',
            'nl':
            'Diepte van het waterpeil ten opzichte van het grondoppervlak',
            'en': '-',
        },
        'unit': 'm',
        'groups': [],
        'norms': [],
    }
    piezo_abs = {
        'id': PIEZO_ABSOLUTE,
        'code': 'abs',
        'name': {
            'fr': 'Piezométrie absolue',
            'nl': 'Absolute piëzometrie',
            'en': '-',
        },
        'unit': 'm',
        'groups': [],
        'norms': [],
    }
    params = [piezo_rel, piezo_abs]

    return JsonResponse(params, safe=False)


def get_timeserie_surface_data(station, parameter, start, end):
    Results = models.surface.Results
    Site = models.surface.Site
    Parameter = models.surface.Parameter
    ser = serializers.surface.serialize_timeserie_record
    param = get_object_or_404(Parameter, pk=parameter)
    site = get_object_or_404(Site, pk=station)
    kwargs = dict(
        id_par_result__id_par=param,
        id_sample__gid_point__gid_site=site,
    )
    if start is not None:
        kwargs['id_sample__sampling_date__gte'] = start
    if end is not None:
        kwargs['id_sample__sampling_date__lte'] = end

    queryset = Results.objects.filter(**kwargs).exclude(
        validated_txt_value='NA').order_by('id_sample__sampling_date')

    return [ser(r) for r in queryset]


def get_timeserie_surface(request, station, parameter):
    query = request.GET
    start = None
    end = None
    if 'start' in query:
        try:
            start = date.fromisoformat(query['start'])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query['start']))
    if 'end' in query:
        try:
            end = date.fromisoformat(query['end'])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query['start']))

    rows = get_timeserie_surface_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_surface(request):
    Site = models.surface.Site
    results = json.loads(
        serialize('geojson',
                  Site.objects.all(),
                  geometry_field='the_geom',
                  srid=31370))
    for feature in results['features']:
        feature['id'] = feature['properties']['pk']

    return JsonResponse(results)


def get_timeserie_ground_quality_data(station, parameter, start, end):
    Results = models.ground.GwQualResultat
    Site = models.ground.GwMnetQual
    Parameter = models.ground.GwQualParametre
    ser = serializers.ground.serialize_timeserie_record_quality
    param = get_object_or_404(Parameter, pk=parameter)
    site = get_object_or_404(Site, no_station=station)
    kwargs = dict(idparametre=param,
                  idechantillon__code_station=site.no_station)
    if start is not None:
        kwargs['idechantillon__date_ech__gte'] = start
    if end is not None:
        kwargs['idechantillon__date_ech__lte'] = end

    queryset = Results.objects.filter(
        **kwargs).order_by('idechantillon__date_ech')

    return [ser(r) for r in queryset]


def get_timeserie_ground_quality(request, station, parameter):
    query = request.GET
    start = None
    end = None
    if 'start' in query:
        try:
            start = date.fromisoformat(query['start'])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query['start']))
    if 'end' in query:
        try:
            end = date.fromisoformat(query['end'])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query['start']))

    rows = get_timeserie_ground_quality_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_ground_quality(request):
    Site = models.ground.GwMnetQualDesc
    results = json.loads(
        serialize('geojson',
                  Site.objects.filter(the_geom__isnull=False),
                  geometry_field='the_geom',
                  srid=31370))
    for feature in results['features']:
        feature['id'] = feature['properties']['no_station']

    return JsonResponse(results)


def get_timeserie_ground_quantity_data(station, parameter, start, end):
    Results = models.ground.piezo_model(station)
    absolute = parameter == PIEZO_ABSOLUTE
    ser = serializers.ground.serialize_timeserie_record_quantity(
        station, absolute)
    prefix = models.ground.make_prefix('data_{}'.format(station))
    time_field = prefix('time')
    value_field = prefix('hp') if absolute else prefix('pfsol')
    kwargs = {'{}__isnull'.format(value_field): False}

    if start is not None:
        kwargs['{}__gte'.format(time_field)] = start
    if end is not None:
        kwargs['{}__lte'.format(time_field)] = end

    queryset = Results.objects.filter(**kwargs).order_by(time_field)

    return [ser(r) for r in queryset]


def get_timeserie_ground_quantity(request, station, parameter):
    if parameter not in [PIEZO_RELATIVE, PIEZO_ABSOLUTE]:
        return HttpResponseBadRequest('piezo is either {} or {}'.format(
            PIEZO_RELATIVE, PIEZO_ABSOLUTE))
    query = request.GET
    start = None
    end = None
    if 'start' in query:
        try:
            start = date.fromisoformat(query['start'])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query['start']))
    if 'end' in query:
        try:
            end = date.fromisoformat(query['end'])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query['start']))

    rows = get_timeserie_ground_quantity_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_ground_quantity(request):
    Site = models.ground.GwMnetPiezoQuantDesc
    results = json.loads(
        serialize('geojson',
                  Site.objects.filter(the_geom__isnull=False,
                                      mon_be_code__isnull=False),
                  geometry_field='the_geom',
                  srid=31370))
    for feature in results['features']:
        feature['id'] = feature['properties']['mon_be_code']

    return JsonResponse(results)


class TSSelector:
    def __init__(self, timeserie):
        self.timeserie = timeserie
        self.index = 0

    def incr_index(self, t):
        """This function is a workaround for duplicate timestamps
        """
        self.index += 1
        if self.index < len(self.timeserie):
            next_row = self.timeserie[self.index]
            next_t = next_row[0]
            if next_t == t:
                self.incr_index(t)

    def select(self, t):
        if self.index < len(self.timeserie):
            row = self.timeserie[self.index]
            if row[0] == t:
                self.incr_index(t)
                return row[1]
            else:
                print('<select {}> index {}, at_index {}'.format(
                    t, self.index, row[0]))

        return ''


def merge_timeseries(timeseries):
    times = []
    for ts in timeseries:
        for record in ts:
            times.append(record[0])

    selectors = [TSSelector(ts) for ts in timeseries]
    results = []
    for t in sorted(set(times)):
        row = [t] + [s.select(t) for s in selectors]
        results.append(row)

    return results


def time_name(lang):
    if lang == 'fr':
        return 'Horodatage'
    elif lang == 'nl':
        return 'Tijdstempel'
    return 'Timestamp'


def get_csv_header_surface(lang, parameters):
    def param_name(p):
        if lang == 'fr':
            return '{} {}'.format(p.par_fr, p.unit_param)
        elif lang == 'nl':
            return '{} {}'.format(p.par_nl, p.unit_param)
        return '{} {}'.format(p.par_en, p.unit_param)

    Parameter = models.surface.Parameter
    params = [get_object_or_404(Parameter, pk=p) for p in parameters]
    return [time_name(lang)] + [param_name(p) for p in params]


def get_csv_header_ground_quality(lang, parameters):
    def param_name(p):
        if lang == 'fr':
            return '{} {}'.format(p.nomparametre, p.uniteparametre)
        elif lang == 'nl':
            return '{} {}'.format(p.naamparametre, p.uniteparametre)
        return '{} {}'.format(p.name_parametre, p.uniteparametre)

    Parameter = models.ground.GwQualParametre
    params = [get_object_or_404(Parameter, pk=p) for p in parameters]
    return [time_name(lang)] + [param_name(p) for p in params]


PIEZO_ABS_NAMES = {
    'fr': 'Niveau piézometrique absolu',
    'nl': 'Absoluut piëzometrisch niveau',
}
PIEZO_REL_NAMES = {
    'fr': 'Niveau piézometrique relatif',
    'nl': 'Relatief piëzometrisch niveau',
}


def get_piezo_name(lang, p):
    if p == PIEZO_ABSOLUTE:
        return PIEZO_ABS_NAMES[lang]
    return PIEZO_REL_NAMES[lang]


def get_csv_header_ground_quantity(lang, parameters):
    return [time_name(lang)] + [get_piezo_name(lang, p) for p in parameters]


def with_lang(lang, obj):
    return obj[lang]


INCR_NAME_RE = re.compile(r'.*_\d+$')


def incr_name(name):
    if INCR_NAME_RE.match(name):
        parts = name.split('_')
        count = int(parts[-1])
        return '{}_{}'.format('_'.join(parts[0:-1]), count + 1)

    return '{}_{}'.format(name, 1)


def make_uniq_path(root, name, ext):
    sname = slugify(name, allow_unicode=True)
    p = root.joinpath('{}.{}'.format(sname, ext))
    if p.exists():
        return make_uniq_path(root, incr_name(name), ext)
    return p


def make_surface_meta(lang, root, start, end, station, timeseries):
    Site = models.surface.Site
    Parameter = models.surface.Parameter
    template_spec = 'surface'
    site = Site.objects.get(pk=station)
    meta_path = make_uniq_path(
        root,
        with_lang(lang, {
            'fr': site.site_fr,
            'nl': site.site_nl,
            'en': site.site_en,
        }), 'html')

    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
    )

    for timeserie, param_id in timeseries:
        parameter = get_object_or_404(Parameter, pk=param_id)
        statistics = get_statistics_surface(
            site,
            parameter,
            timeserie,
        )
        context['parameters'].append(
            dict(parameter=parameter, statistics=statistics))

    rendered = render_to_string(
        'water/meta_{}_{}.html'.format(template_spec, lang), context)

    with meta_path.open('x') as f:
        f.write(rendered)

    return meta_path


def make_ground_quality_meta(lang, root, start, end, station, timeseries):
    Site = models.ground.GwMnetQualDesc
    Parameter = models.ground.GwQualParametre
    template_spec = 'ground-quality'
    site = get_object_or_404(Site, no_station=station)
    meta_path = make_uniq_path(root, site.no_code, 'html')

    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
    )

    for timeserie, param_id in timeseries:
        parameter = get_object_or_404(Parameter, pk=param_id)
        statistics = get_statistics_ground_quality(
            site,
            parameter,
            timeserie,
        )
        context['parameters'].append(
            dict(parameter=parameter, statistics=statistics))

    rendered = render_to_string(
        'water/meta_{}_{}.html'.format(template_spec, lang), context)

    with meta_path.open('x') as f:
        f.write(rendered)

    return meta_path


def make_ground_quantity_meta(lang, root, start, end, station, timeseries):
    Site = models.ground.GwMnetPiezoQuantDesc
    template_spec = 'ground-quantity'
    site = get_object_or_404(Site, mon_be_code=station)
    meta_path = make_uniq_path(root, site.nom, 'html')

    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
    )

    for timeserie, param_id in timeseries:
        parameter = get_piezo_name(lang, param_id)
        statistics = get_statistics_ground_quantity(
            site,
            timeserie,
        )
        context['parameters'].append(
            dict(parameter=parameter, statistics=statistics))

    rendered = render_to_string(
        'water/meta_{}_{}.html'.format(template_spec, lang), context)

    with meta_path.open('x') as f:
        f.write(rendered)

    return meta_path


# def tempdir():
#     root = PosixPath('/tmp')
#     name = 'water-' + ''.join(
#         random.choice(string.ascii_letters) for i in range(12))
#     tmp = root.joinpath(name)
#     tmp.mkdir()
#     return tmp


def package_csv(config, lang, get_name, get_timeserie, get_header, make_meta):
    kind = config.kind
    stations = config.stations
    parameters = config.parameters
    start = config.start
    end = config.end
    station_names = []

    with tempfile.TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        sheets = []
        metas = []
        for station in stations:
            name = get_name(lang, station)
            station_names.append(name)
            sheet_path = make_uniq_path(root, name, 'csv')
            timeseries = []
            for parameter in parameters:
                ts = get_timeserie(station, parameter, start, end)
                timeseries.append((ts, parameter))

            with sheet_path.open('x') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow(get_header(lang, parameters))
                writer.writerows(merge_timeseries([ts[0]
                                                   for ts in timeseries]))

            sheets.append(sheet_path)

            metas.append(make_meta(lang, root, start, end, station,
                                   timeseries))

        # here me have to avoid too long names when many stations are selected
        extra = '' if len(station_names) == 1 else '-{}'.format(''.join(
            map(lambda x: slugify(x, True)[0], station_names[1:])))
        zip_name = '{}{}_{}.zip'.format(slugify(station_names[0], True), extra,
                                        date.today())
        zip_path = root.joinpath(zip_name)
        with zipfile.ZipFile(zip_path.as_posix(), mode='x') as zip:
            for sheet_path in sheets:
                zip.write(sheet_path.as_posix(),
                          sheet_path.relative_to(root).as_posix())
            for meta_path in metas:
                zip.write(meta_path.as_posix(),
                          meta_path.relative_to(root).as_posix())

        return FileResponse(zip_path.open('rb'),
                            as_attachment=True,
                            filename=zip_name)


def get_surface_name(lang, station):
    site = models.surface.Site.objects.get(pk=station)
    return with_lang(lang, {
        'fr': site.site_fr,
        'nl': site.site_nl,
        'en': site.site_en,
    })


def get_ground_qual_name(lang, station):
    site = get_object_or_404(models.ground.GwMnetQualDesc, no_station=station)
    return site.no_code


def get_ground_quant_name(lang, station):
    site = get_object_or_404(models.ground.GwMnetPiezoQuantDesc,
                             mon_be_code=station)
    return site.nom


def get_csv(request, lang, config):
    parsed = parse_config(config)
    print('>> config')
    print(str(parsed))
    kind = parsed.kind
    if kind == 'surface':
        return package_csv(parsed, lang, get_surface_name,
                           get_timeserie_surface_data, get_csv_header_surface,
                           make_surface_meta)
    elif kind == 'ground-quality':
        return package_csv(parsed, lang, get_ground_qual_name,
                           get_timeserie_ground_quality_data,
                           get_csv_header_ground_quality,
                           make_ground_quality_meta)
    if kind == 'ground-quantity':
        return package_csv(parsed, lang, get_ground_quant_name,
                           get_timeserie_ground_quantity_data,
                           get_csv_header_ground_quantity,
                           make_ground_quantity_meta)

    return HttpResponseBadRequest('Unknown kind: "{}"'.format(kind))


def get_norm_names(request):
    records = models.surface.NormNames.objects.all()
    result = {}
    for rec in records:
        name = rec.norm_names
        if name is not None:
            result[name] = {
                'fr': rec.fr if not None else name,
                'nl': rec.nl if not None else name,
                'en': rec.en if not None else name,
            }

    return JsonResponse(result)


def get_water_bodies(request):
    ground_records = models.ground.GwHgeolTgwb.objects.all()
    surface_records = models.surface.Waterbody.objects.all()
    result = []
    for rec in ground_records:
        result.append({
            'kind': 'ground',
            'id': rec.id_gwb,
            'name': {
                'fr': rec.nom_gwb_fr,
                'nl': rec.naam_gwb_nl,
            },
            'code': rec.code_gwb,
        })
    for rec in surface_records:
        result.append({
            'kind': 'surface',
            'id': rec.gid,
            'name': {
                'fr': rec.waterbody_fr,
                'nl': rec.waterbody_nl,
            },
            'code': rec.id_waterbody,
        })

    return JsonResponse(result, safe=False)
