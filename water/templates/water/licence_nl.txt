# Naamsvermelding 4.0 Internationaal (CC BY 4.0)

Dit is de vereenvoudigde (human-readable) versie van de volledige [licentie](https://creativecommons.org/licenses/by/4.0/legalcode) en geen vervanging van de volledige licentie. 

## Je bent vrij om:

**het werk te delen** — te kopiëren, te verspreiden en door te geven via elk medium of bestandsformaat

**het werk te bewerken** — te remixen, te veranderen en afgeleide werken te maken
voor alle doeleinden, inclusief commerciële doeleinden.

De licentiegever kan deze toestemming niet intrekken zolang aan de licentievoorwaarden voldaan wordt.


## Onder de volgende voorwaarden:

**Naamsvermelding** — De gebruiker dient de maker van het werk te vermelden, een link naar de licentie te plaatsen en aan te geven of het werk veranderd is. Je mag dat op redelijke wijze doen, maar niet zodanig dat de indruk gewekt wordt dat de licentiegever instemt met je werk of je gebruik van het werk.

**Geen aanvullende restricties** — Je mag geen juridische voorwaarden of technologische voorzieningen toepassen die anderen er juridisch in beperken om iets te doen wat de licentie toestaat.

## Let op:

Voor elementen van het materiaal die zich in het publieke domein bevinden, en voor vormen van gebruik die worden toegestaan via een uitzondering of beperking in de Auteurswet, hoef je je niet aan de voorwaarden van de licentie te houden.

Er worden geen garanties afgegeven. Het is mogelijk dat de licentie je niet alle gebruiksvrijheden geeft die nodig zijn voor het beoogde gebruik. Bijvoorbeeld, andere rechten zoals publiciteits-, privacy- en morele rechten kunnen het gebruik van een werk beperken.