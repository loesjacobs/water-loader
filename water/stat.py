from .serializers import surface as ser_surface
from .serializers import ground as ser_ground
import functools
from statistics import stdev, mean

## {{{ http://code.activestate.com/recipes/511478/ (r1)
import math


def percentile(N, percent, key=lambda x: x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N) - 1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c - k)
    d1 = key(N[int(c)]) * (k - f)
    return d0 + d1


## end of http://code.activestate.com/recipes/511478/ }}}

p10 = functools.partial(percentile, percent=0.1)
p50 = functools.partial(percentile, percent=0.5)
p90 = functools.partial(percentile, percent=0.9)


def base_statistics(values):
    """
    n
    min
    max
    P10
    P50
    P90
    avg
    stdev
    """
    if len(values) == 0:
        return {
            'n': 0,
            'min': '-',
            'max': '-',
            'p10': '-',
            'p50': '-',
            'p90': '-',
            'avg': '-',
            'stdev': '-',
        }
    elif len(values) == 1:
        val = values[0]
        return {
            'n': 1,
            'min': val,
            'max': val,
            'p10': val,
            'p50': val,
            'p90': val,
            'avg': val,
            'stdev': 0,
        }

    return {
        'n': len(values),
        'min': min(values),
        'max': max(values),
        'p10': p10(values),
        'p50': p50(values),
        'p90': p90(values),
        'avg': mean(values),
        'stdev': stdev(values),
    }


# SET etat = 'good'
# WHERE (norms_aa_eu is not NULL and norms_aa_eu > avg)
# OR (norms_aa_bcr is not NULL and norms_aa_bcr > avg)
# OR (norms_sum is not NULL and norms_sum > avg)
# OR (norms_mac is not NULL and norms_mac > max)
# OR (norms_min_val is not NULL and norms_min_val < min)
# OR (norms_max_val is not NULL and norms_max_val > max)
# OR (norms_aa_min_val is not NULL and norms_aa_min_val < avg)
# OR (norms_p90 is not NULL and norms_p90 > p90)
# OR (norms_p10 is not NULL and norms_p10 < p10)
# OR (norms_max_count is not NULL and norms_max_count > count) ;

more_than_avg = lambda ctx, n: n > ctx['avg']
less_than_avg = lambda ctx, n: n < ctx['avg']
less_than_min = lambda ctx, n: n < ctx['min']
more_than_max = lambda ctx, n: n > ctx['max']
more_than_p90 = lambda ctx, n: n > ctx['p90']
less_than_p10 = lambda ctx, n: n < ctx['p10']
NORMS = {
    'aa_bcr': more_than_avg,
    'aa_eu': more_than_avg,
    'sum': more_than_avg,
    'mac': more_than_max,
    'min_val': less_than_min,
    'max_val': more_than_max,
    'aa_min_val': less_than_avg,
    # 'biote': ,
    # 'summer': ,
    # 'winter',
    'p10': less_than_p10,
    'p90': more_than_p90,
    ## TODO: GROUND, to check
    'eaupotable': more_than_avg,
    'dce_nqe': more_than_avg,
}


def eval_norms_surface(site, norms, ctx):
    wb = norms['options']['waterbody']
    vn = []
    if wb == site.id_waterbody_id:
        for key in norms['values']:
            op = NORMS[key]
            val = norms['values'][key]
            try:
                vn.append(dict(name=key, conform=op(ctx, val)))
            except Exception:
                pass
        return vn

    return None


def eval_norms_ground(site, norms, ctx):
    wb = norms['options'].get('waterbody')
    vn = []
    if wb is None or wb == site.ref_gwb:
        for key in norms['values']:
            op = NORMS[key]
            val = norms['values'][key]
            try:
                vn.append(dict(name=key, conform=op(ctx, val)))
            except Exception:
                pass
        return vn

    return None


def get_statistics_surface(site, parameter, timeserie):
    values = [rec[1] * rec[2] for rec in timeserie]
    ctx = base_statistics(values)

    if len(values) > 0:
        pr = parameter.parresult_set.first()  # fragile but it works
        ctx['norms'] = [
            eval_norms_surface(site, ser_surface.serialize_norm(n), ctx)
            for n in pr.norms_set.filter(end_date__isnull=True)
            if n is not None
        ]
    else:
        ctx['norms'] = []

    return ctx


def get_statistics_ground_quality(site, parameter, timeserie):
    values = [rec[1] * rec[2] for rec in timeserie]
    ctx = base_statistics(values)

    if len(values) > 0:
        ctx['norms'] = [
            eval_norms_ground(site, n, ctx)
            for n in ser_ground.serialize_norm(parameter) if n is not None
        ]
    else:
        ctx['norms'] = []

    return ctx


def binary_search(min, max, predicate):
    interval = max - min
    pivot = min + math.floor(interval / 2)

    if max == min:
        return pivot
    elif max < min:
        return pivot

    pred_res = predicate(pivot)

    if pred_res > 0:
        return binary_search(min, pivot, predicate)
    elif pred_res < 0:
        return binary_search(pivot + 1, max, predicate)
    return pivot


def extract_year(iso_string):
    return int(iso_string[0:4])


def extract_month(iso_string):
    return int(iso_string[5:7])


class Yearly:
    def __init__(self, year, timeserie):
        def predicate(index):
            if index <= 0:
                return 0
            dt_year = extract_year(timeserie[index][0])
            dt_year2 = extract_year(timeserie[index - 1][0])
            if dt_year2 < dt_year:
                # print('<pred {}> found at index {}; month {}'.format(
                #     year, index, extract_month(timeserie[index][0])))
                return 0

            if dt_year < year:
                return -1

            return 1

        self.year = year
        self.timeserie = timeserie
        self.max_index = len(timeserie) - 1
        self.index = binary_search(0, self.max_index, predicate)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index > self.max_index:
            raise StopIteration()

        cur_record = self.timeserie[self.index]
        cur_year = extract_year(cur_record[0])
        if cur_year == self.year:
            self.index += 1
            return cur_record

        raise StopIteration()

    def __str__(self):
        return '<Yearly({}): [{}; {}]>'.format(self.year, self.index,
                                               self.max_index)


def split_years(timeserie):
    start_year = extract_year(timeserie[0][0])
    end_year = extract_year(timeserie[-1][0])
    series = []
    for year in range(start_year, end_year + 1):
        series.append(Yearly(year, timeserie))

    return series


def get_trend(v_start, v_end):
    trend = '→'
    if v_end > v_start:
        trend = '↗'
    if v_start > v_end:
        trend = '↘'
    return trend


class NotFound(Exception):
    pass


def find(fn, serie):
    for r in serie:
        if fn(r):
            return r
    raise NotFound()


def get_monthly_statistics(serie):
    first_day = None
    cur_day = None
    cur_month = None
    cur_data = []
    results = []
    for rec in serie:
        month = extract_month(rec[0])
        if first_day is None:
            first_day = rec
            cur_day = rec
            cur_month = month
            cur_data = [rec[1] * rec[2]]
        else:
            if month != cur_month:
                try:
                    results.append({
                        'month':
                        cur_month,
                        'avg':
                        mean(cur_data),
                        'trend':
                        get_trend(first_day[1], cur_day[1]),
                    })
                except Exception:
                    pass

                first_day = rec
                cur_day = rec
                cur_month = month
                cur_data = [rec[1] * rec[2]]
            else:
                cur_day = rec
                cur_data.append(rec[1] * rec[2])

    # last month in the serie
    try:
        results.append({
            'month': cur_month,
            'avg': mean(cur_data),
            'trend': get_trend(first_day[1], cur_day[1]),
        })
    except Exception:
        pass

    ret = []
    for m in range(1, 13):
        try:
            ret.append(find(lambda s: s['month'] == m, results))
        except NotFound:
            ret.append({
                'month': m,
                'avg': '×',
                'trend': '×',
            })
    return ret


def get_statistics_ground_quantity(site, timeserie):
    if len(timeserie) > 1:
        series = split_years(timeserie)
        results = []
        for serie in series:
            records = list(serie)
            values = [rec[1] * rec[2] for rec in records]
            if len(values) > 1:
                if len(values) > 1:
                    results.append({
                        'year': serie.year,
                        'n': len(values),
                        'min': min(values),
                        'max': max(values),
                        'avg': mean(values),
                        'trend': get_trend(values[0], values[-1]),
                        'monthly': get_monthly_statistics(records)
                    })

        return results

    return []
